package com.example.pruebatecnica.service_imp;

import com.example.pruebatecnica.dto.CandidateDto;
import com.example.pruebatecnica.dto.CandidateExperienceProjection;
import com.example.pruebatecnica.dto.CandidatesAndYearsExperienceDto;
import com.example.pruebatecnica.dto.TechnologyDto;
import com.example.pruebatecnica.exception.PersonalizeException;
import com.example.pruebatecnica.mapper.CandidateMapper;
import com.example.pruebatecnica.mapper.TechnologyMapper;
import com.example.pruebatecnica.model.Candidate;
import com.example.pruebatecnica.model.CandidateTechnology;
import com.example.pruebatecnica.model.Technology;
import com.example.pruebatecnica.repository.CandidateRepository;
import com.example.pruebatecnica.repository.CandidateTechnologyRepository;
import com.example.pruebatecnica.repository.TechnologyRepository;
import com.example.pruebatecnica.service.TechnologyService;
import com.example.pruebatecnica.service.service_imp.TechnologyServiceImp;
import jakarta.persistence.EntityNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.internal.verification.VerificationModeFactory.times;

@ExtendWith(MockitoExtension.class)
class TechnologyServiceImpTest {

    @Mock
    private TechnologyRepository technologyRepository;

    @Mock
    private TechnologyMapper technologyMapper;

    @Mock
    private CandidateRepository candidateRepository;

    @Mock
    private CandidateTechnologyRepository candidateTechnologyRepository;


    @Mock
    CandidateMapper candidateMapper;

    @InjectMocks
    private TechnologyServiceImp technologyService;

    private TechnologyDto technologyDto;

    private Technology technology;

    private Candidate candidate;

    private CandidateDto candidateDto;

    @BeforeEach
    public void init() {
        technologyDto = TechnologyDto.builder()
                .name("Java")
                .version("11")
                .build();

        technology = Technology.builder()
                .id(1L)
                .name("Java")
                .version("11")
                .build();

        candidateDto = CandidateDto.builder()
                .name("Maximiliano")
                .lastName("Monje")
                .documentType("DNI")
                .documentNumber("40428769")
                .birthDate(LocalDate.of(1997, 7, 16))
                .build();

        candidate = Candidate.builder()
                .id(1L)
                .name("Maximiliano")
                .lastName("Monje")
                .documentType("DNI")
                .documentNumber("40428769")
                .birthDate(LocalDate.of(1997, 7, 16))
                .build();
    }

    @Test
    void getTechnology_WhenExists() {


        when(technologyRepository.findById(technology.getId())).thenReturn(Optional.of(technology));
        when(technologyMapper.technologyToTechnologyDto(any(Technology.class))).thenReturn(technologyDto);

        TechnologyDto result = technologyService.getTechnology(technology.getId());

        assertNotNull(result);
        assertEquals("Java", result.getName());
        assertEquals("11", result.getVersion());

        verify(technologyRepository).findById(technology.getId());
        verify(technologyMapper).technologyToTechnologyDto(technology);
    }

    @Test
    void getTechnology_WhenNotExists() {
        Long id = 2L;
        when(technologyRepository.findById(id)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> technologyService.getTechnology(id));

        verify(technologyRepository).findById(id);

    }

    @Test
    void getAllTechnologies_WhenNotEmpty() {

        List<Technology> technologies = new ArrayList<>();
        technologies.add(technology);
        technologies.add(Technology.builder()
                .id(2L)
                .name("Python")
                .version("3.8")
                .build());
        List<TechnologyDto> technologiesDto =new ArrayList<>();
        technologiesDto.add(technologyDto);
        technologiesDto.add(TechnologyDto.builder()
                .name("Python")
                .version("3.8")
                .build());


        when(technologyRepository.findAll()).thenReturn(technologies);
        when(technologyMapper.technologyToTechnologyDto(technologies.get(0))).thenReturn(technologiesDto.get(0));
        when(technologyMapper.technologyToTechnologyDto(technologies.get(1))).thenReturn(technologiesDto.get(1));

        List<TechnologyDto> result = technologyService.getAllTechnologies();

        assertNotNull(result);
        assertEquals(technologiesDto.size(), result.size());

        for (int i = 0; i < technologiesDto.size(); i++) {
            assertEquals(technologiesDto.get(i).getName(), result.get(i).getName());
            assertEquals(technologiesDto.get(i).getVersion(), result.get(i).getVersion());
        }

        verify(technologyRepository).findAll();
        verify(technologyMapper, times(technologies.size())).technologyToTechnologyDto(any(Technology.class));
    }

    @Test
    void getAllTechnologies_WhenEmpty() {

        when(technologyRepository.findAll()).thenReturn(Collections.emptyList());
        assertThrows(EntityNotFoundException.class, () -> technologyService.getAllTechnologies());


        verify(technologyRepository).findAll();

    }

    @Test
    void deleteTechnology_WhenExists() {


        when(technologyRepository.findById(technology.getId())).thenReturn(Optional.of(technology));

        technologyService.deleteTechnology(technology.getId());

        verify(technologyRepository).findById(technology.getId());
        verify(technologyRepository).deleteById(technology.getId());
    }

    @Test
    void deleteTechnology_WhenNotExists() {
        Long id = 2L;
        when(technologyRepository.findById(id)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> technologyService.deleteTechnology(id));

        verify(technologyRepository).findById(id);
        verify(technologyRepository, never()).deleteById(id);
    }


    @Test
    void saveTechnology_WhenValidData() {


        when(technologyMapper.technologyDtoToTechnology(null, technologyDto)).thenReturn(technology);
        when(technologyRepository.existsByNameAndVersion("Java", "11")).thenReturn(false);
        when(technologyRepository.save(any(Technology.class))).thenReturn(technology);

        technologyService.saveTechnology(technologyDto);

        verify(technologyRepository).save(technology);
        verify(technologyMapper).technologyDtoToTechnology(null, technologyDto);
        verify(technologyRepository).existsByNameAndVersion("Java", "11");
    }

    @Test
    void saveTechnology_WhenNameIsEmpty() {
        TechnologyDto technologyDtoNew = new TechnologyDto("", "8");

        Exception exception = assertThrows(PersonalizeException.class, () -> technologyService.saveTechnology(technologyDtoNew));

        assertEquals("El nombre de la tecnología es obligatorio", exception.getMessage());
    }

    @Test
    void saveTechnology_WhenVersionIsEmpty() {
        TechnologyDto technologyDtoNew = new TechnologyDto("Java", "");

        Exception exception = assertThrows(PersonalizeException.class, () -> technologyService.saveTechnology(technologyDtoNew));

        assertEquals("La versión de la tecnología es obligatoria", exception.getMessage());
    }

    @Test
    void saveTechnology_WhenTechnologyExists() {
        TechnologyDto technologyDtoNew = new TechnologyDto("Java", "11");

        when(technologyRepository.existsByNameAndVersion("Java", "11")).thenReturn(true);

        Exception exception = assertThrows(PersonalizeException.class, () -> technologyService.saveTechnology(technologyDtoNew));

        assertEquals("Ya existe una tecnología con el mismo nombre y versión.", exception.getMessage());
    }

    @Test
    void updateTechnology_WhenValidData() {


        when(technologyRepository.findById(technology.getId())).thenReturn(Optional.of(technology));
        when(technologyRepository.existsByNameAndVersion(technologyDto.getName(), technologyDto.getVersion())).thenReturn(false);
        when(technologyRepository.save(any(Technology.class))).thenReturn(technology);

        technologyService.updateTechnology(technology.getId(), technologyDto);

        verify(technologyRepository).save(technology);
        assertEquals("Java", technology.getName());
        assertEquals("11", technology.getVersion());
    }

    @Test
    void updateTechnology_WhenTechnologyNotFound() {
        Long id = 2L;
        when(technologyRepository.findById(id)).thenReturn(Optional.empty());

        Exception exception = assertThrows(EntityNotFoundException.class, () -> technologyService.updateTechnology(id, technologyDto));

        assertEquals("No se encontró una tecnología con ese ID", exception.getMessage());
        verify(technologyRepository, never()).save(any(Technology.class));
    }

    @Test
    void updateTechnology_WhenDuplicateNameAndVersion() {
        Long technologyId = 1L;
        TechnologyDto technologyDtoNew = new TechnologyDto("Java", "11");

        when(technologyRepository.findById(technology.getId())).thenReturn(Optional.of(technology));
        when(technologyRepository.existsByNameAndVersion(technologyDtoNew.getName(), technologyDtoNew.getVersion())).thenReturn(true);

        Exception exception = assertThrows(PersonalizeException.class, () -> technologyService.updateTechnology(technologyId, technologyDtoNew));

        assertEquals("Ya existe una tecnología con el mismo nombre y versión.", exception.getMessage());
        verify(technologyRepository, never()).save(any(Technology.class));
    }


    @Test
    void addCandidate_WhenValidData() {
        Long candidateId = 1L;

        CandidateTechnology candidateTechnologyYears = new CandidateTechnology();
        candidateTechnologyYears.setYearsOfExperience(5);

        technology.setCandidates(new ArrayList<>());
        Candidate candidate = new Candidate();

        when(technologyRepository.findById(technology.getId())).thenReturn(Optional.of(technology));
        when(candidateRepository.findById(candidateId)).thenReturn(Optional.of(candidate));
        when(candidateTechnologyRepository.existsByCandidateAndTechnology(candidate, technology)).thenReturn(false);

        technologyService.addCandidate(technology.getId(), candidateId, candidateTechnologyYears);

        verify(candidateTechnologyRepository).save(any(CandidateTechnology.class));

    }

    @Test
    void addCandidate_WhenYearsOfExperienceIsNegative() {
        Long candidateId = 1L;
        Long technologyId = 1L;
        CandidateTechnology candidateTechnologyYears = new CandidateTechnology();
        candidateTechnologyYears.setYearsOfExperience(-1);

        when(technologyRepository.findById(technology.getId())).thenReturn(Optional.of(new Technology()));
        when(candidateRepository.findById(candidateId)).thenReturn(Optional.of(new Candidate()));

        assertThrows(PersonalizeException.class, () -> technologyService.addCandidate(technologyId, candidateId, candidateTechnologyYears));
    }

    @Test
    void addCandidate_WhenTechnologyNotFound() {
        Long candidateId = 1L;
        Long technologyId = 1L;
        CandidateTechnology candidateTechnologyYears = new CandidateTechnology();

        when(technologyRepository.findById(technology.getId())).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> technologyService.addCandidate(technologyId, candidateId, candidateTechnologyYears));
    }

    @Test
    void addCandidate_WhenCandidateNotFound() {
        Long candidateId = 1L;
        Long technologyId = 1L;
        CandidateTechnology candidateTechnologyYears = new CandidateTechnology();

        when(technologyRepository.findById(technology.getId())).thenReturn(Optional.of(new Technology()));
        when(candidateRepository.findById(candidateId)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> technologyService.addCandidate(technologyId, candidateId, candidateTechnologyYears));
    }

    @Test
    void addCandidate_WhenCombinationExists() {
        Long candidateId = 1L;
        Long technologyId= 1L;
        CandidateTechnology candidateTechnologyYears = new CandidateTechnology();
        Candidate candidate = new Candidate();

        when(technologyRepository.findById(technology.getId())).thenReturn(Optional.of(technology));
        when(candidateRepository.findById(candidateId)).thenReturn(Optional.of(candidate));
        when(candidateTechnologyRepository.existsByCandidateAndTechnology(candidate, technology)).thenReturn(true);

        assertThrows(PersonalizeException.class, () -> technologyService.addCandidate(technologyId, candidateId, candidateTechnologyYears));
    }
    @Test
    void getCandidatesAndExperience_WhenFound() {
        List<CandidateExperienceProjection> projections = new ArrayList<>();
        CandidateExperienceProjection projection = mock(CandidateExperienceProjection.class);


        projections.add(projection);

        when(candidateTechnologyRepository.findCandidatesAndExperience(technology.getId())).thenReturn(projections);
        when(projection.getCandidate()).thenReturn(candidate);
        when(projection.getYearsOfExperience()).thenReturn(5);
        when(candidateMapper.candidateToCandidateDto(candidate)).thenReturn(candidateDto);

        List<CandidatesAndYearsExperienceDto> result = technologyService.getCandidatesAndExperience(technology.getId());

        assertNotNull(result);
        assertEquals(1, result.size());
        assertEquals("Maximiliano", result.get(0).getCandidateDto().getName());
        assertEquals("Monje", result.get(0).getCandidateDto().getLastName());
        assertEquals("DNI", result.get(0).getCandidateDto().getDocumentType());
        assertEquals("40428769", result.get(0).getCandidateDto().getDocumentNumber());
        assertEquals(5, result.get(0).getYearsExperience());

        verify(candidateTechnologyRepository).findCandidatesAndExperience(technology.getId());
        verify(candidateMapper).candidateToCandidateDto(candidate);
    }

    @Test
    void getCandidatesAndExperience_WhenNotFound() {
        Long technologyId = 2L;

        when(candidateTechnologyRepository.findCandidatesAndExperience(technologyId)).thenReturn(new ArrayList<>());

        assertThrows(EntityNotFoundException.class, () -> technologyService.getCandidatesAndExperience(technologyId));
    }

    @Test
    void getCandidatesAndExperienceNV_WhenFound() {
        List<CandidateExperienceProjection> projections = new ArrayList<>();
        CandidateExperienceProjection projection = mock(CandidateExperienceProjection.class);



        projections.add(projection);

        when(candidateTechnologyRepository.findCandidatesAndExperienceNV(technologyDto.getName(), technologyDto.getVersion())).thenReturn(projections);
        when(projection.getCandidate()).thenReturn(candidate);
        when(projection.getYearsOfExperience()).thenReturn(5);
        when(candidateMapper.candidateToCandidateDto(candidate)).thenReturn(candidateDto);

        List<CandidatesAndYearsExperienceDto> result = technologyService.getCandidatesAndExperienceNV(technologyDto);

        assertNotNull(result);
        assertEquals(1, result.size());
        assertEquals("Maximiliano", result.get(0).getCandidateDto().getName());
        assertEquals("Monje", result.get(0).getCandidateDto().getLastName());
        assertEquals("DNI", result.get(0).getCandidateDto().getDocumentType());
        assertEquals("40428769", result.get(0).getCandidateDto().getDocumentNumber());
        assertEquals(5, result.get(0).getYearsExperience());

        verify(candidateTechnologyRepository).findCandidatesAndExperienceNV(technologyDto.getName(), technologyDto.getVersion());
        verify(candidateMapper).candidateToCandidateDto(candidate);
    }

    @Test
    void getCandidatesAndExperienceNV_WhenNotFound() {
        when(candidateTechnologyRepository.findCandidatesAndExperienceNV(technologyDto.getName(), technologyDto.getVersion())).thenReturn(new ArrayList<>());

        assertThrows(EntityNotFoundException.class, () -> technologyService.getCandidatesAndExperienceNV(technologyDto));

        verify(candidateTechnologyRepository).findCandidatesAndExperienceNV(technologyDto.getName(), technologyDto.getVersion());
    }
}
