package com.example.pruebatecnica.service_imp;

import com.example.pruebatecnica.dto.*;
import com.example.pruebatecnica.exception.PersonalizeException;
import com.example.pruebatecnica.mapper.CandidateMapper;
import com.example.pruebatecnica.mapper.TechnologyMapper;
import com.example.pruebatecnica.model.Candidate;
import com.example.pruebatecnica.model.CandidateTechnology;
import com.example.pruebatecnica.model.Technology;
import com.example.pruebatecnica.repository.CandidateRepository;
import com.example.pruebatecnica.repository.CandidateTechnologyRepository;
import com.example.pruebatecnica.repository.TechnologyRepository;
import com.example.pruebatecnica.service.service_imp.CandidateServiceImp;
import jakarta.persistence.EntityNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.internal.verification.VerificationModeFactory.times;

@ExtendWith(MockitoExtension.class)
class CandidateServiceImpTest {

    @Mock
    private CandidateRepository candidateRepository;

    @Mock
    private CandidateMapper candidateMapper;

    @Mock
    private CandidateTechnologyRepository candidateTechnologyRepository;

    @Mock
    private TechnologyRepository technologyRepository;

    @Mock
    TechnologyMapper technologyMapper;

    @InjectMocks
    private CandidateServiceImp candidateService;

    private Candidate candidate;

    private CandidateDto candidateDto;

    private TechnologyDto technologyDto;

    private Technology technology;


    @BeforeEach
    public void init() {
        candidateDto = CandidateDto.builder()
                .name("Maximiliano")
                .lastName("Monje")
                .documentType("DNI")
                .documentNumber("40428769")
                .birthDate(LocalDate.of(1997, 7, 16))
                .build();

        candidate = Candidate.builder()
                .id(1L)
                .name("Maximiliano")
                .lastName("Monje")
                .documentType("DNI")
                .documentNumber("40428769")
                .birthDate(LocalDate.of(1997, 7, 16))
                .build();

        technologyDto = TechnologyDto.builder()
                .name("Java")
                .version("11")
                .build();

        technology = Technology.builder()
                .id(1L)
                .name("Java")
                .version("11")
                .build();
    }

    @Test
    void getCandidate_WhenExists(){

        when(candidateRepository.findById(candidate.getId())).thenReturn(Optional.of(candidate));
        when(candidateMapper.candidateToCandidateDto(any(Candidate.class))).thenReturn(candidateDto);

        CandidateDto result = candidateService.getCandidate(candidate.getId());

        assertNotNull(result);
        assertEquals("Maximiliano", result.getName());
        assertEquals("Monje", result.getLastName());
        assertEquals("DNI", result.getDocumentType());
        assertEquals("40428769", result.getDocumentNumber());
        assertEquals(LocalDate.of(1997, 7, 16), result.getBirthDate());

        verify(candidateRepository).findById(candidate.getId());
        verify(candidateMapper).candidateToCandidateDto(candidate);
    }

    @Test
    void getCandidate_WhenNotExists(){
        Long id = 2L;
        when(candidateRepository.findById(id)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, ()->candidateService.getCandidate(id));
    }

    @Test
    void getAllCandidates_WhenNotEmpty(){

        List<Candidate> candidates =  new ArrayList<>();
        candidates.add(candidate);
        candidates.add(Candidate.builder()
                .id(2L)
                .name("Gabriel")
                .lastName("Smith")
                .documentType("LC")
                .documentNumber("30328769")
                .birthDate(LocalDate.of(1977, 2, 20))
                .build());

        List<CandidateDto> candidatesDto = new ArrayList<>();
        candidatesDto.add(candidateDto);
        candidatesDto.add(CandidateDto.builder()
                .name("Gabriel")
                .lastName("Smith")
                .documentType("LC")
                .documentNumber("30328769")
                .birthDate(LocalDate.of(1977, 2, 20))
                .build());



        when(candidateRepository.findAll()).thenReturn(candidates);
        when(candidateMapper.candidateToCandidateDto(candidates.get(0))).thenReturn(candidatesDto.get(0));
        when(candidateMapper.candidateToCandidateDto(candidates.get(1))).thenReturn(candidatesDto.get(1));

        List<CandidateDto> result = candidateService.getAllCandidates();

        assertNotNull(result);
        assertEquals(candidatesDto.size(), result.size());

        for (int i = 0; i < candidatesDto.size(); i++) {
            assertEquals(candidatesDto.get(i).getName(), result.get(i).getName());
            assertEquals(candidatesDto.get(i).getLastName(), result.get(i).getLastName());
            assertEquals(candidatesDto.get(i).getDocumentType(), result.get(i).getDocumentType());
            assertEquals(candidatesDto.get(i).getDocumentNumber(), result.get(i).getDocumentNumber());
            assertEquals(candidatesDto.get(i).getBirthDate(), result.get(i).getBirthDate());
        }

        verify(candidateRepository).findAll();
        verify(candidateMapper, times(candidates.size())).candidateToCandidateDto(any(Candidate.class));
    }

    @Test
    void getAllCandidates_WhenEmpty() {

        when(candidateRepository.findAll()).thenReturn(Collections.emptyList());
        assertThrows(EntityNotFoundException.class, () -> candidateService.getAllCandidates());

    }


    @Test
    void deleteCandidate_WhenExists(){

        when(candidateRepository.findById(candidate.getId())).thenReturn(Optional.of(candidate));

        candidateService.deleteCandidate(candidate.getId());

        verify(candidateRepository).findById(candidate.getId());
        verify(candidateRepository).deleteById(candidate.getId());
    }

    @Test
    void deleteTechnology_WhenNotExists() {
        Long id = 2L;
        when(candidateRepository.findById(id)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> candidateService.deleteCandidate(id));

        verify(candidateRepository).findById(id);
        verify(candidateRepository, never()).deleteById(id);
    }


    @Test
    void saveCandidate_WhenValidData() {

        when(candidateMapper.candidateDtoToCandidate(null, candidateDto)).thenReturn(candidate);
        when(candidateRepository.save(any(Candidate.class))).thenReturn(candidate);

        candidateService.saveCandidate(candidateDto);

        verify(candidateRepository).save(candidate);
        verify(candidateMapper).candidateDtoToCandidate(null, candidateDto);

    }

    @Test
    void saveCandidate_WhenNameIsEmpty() {
        CandidateDto candidateDtoNew =  CandidateDto.builder()
                .name("")
                .lastName("Monje")
                .documentType("DNI")
                .documentNumber("40428769")
                .birthDate(LocalDate.of(1997, 7, 16))
                .build();

        Exception exception = assertThrows(PersonalizeException.class, () -> candidateService.saveCandidate(candidateDtoNew));

        assertEquals("El nombre del candidato es obligatorio", exception.getMessage());
    }

    @Test
    void saveCandidate_WhenLastNameIsEmpty() {
        CandidateDto candidateDtoNew =  CandidateDto.builder()
                .name("Maximiliano")
                .lastName("")
                .documentType("DNI")
                .documentNumber("40428769")
                .birthDate(LocalDate.of(1997, 7, 16))
                .build();

        Exception exception = assertThrows(PersonalizeException.class, () -> candidateService.saveCandidate(candidateDtoNew));

        assertEquals("El apellido del candidato es obligatorio", exception.getMessage());
    }

    @Test
    void saveCandidate_WhenDocumentTypeIsEmpty() {
        CandidateDto candidateDtoNew =  CandidateDto.builder()
                .name("Maximiliano")
                .lastName("Monje")
                .documentType("")
                .documentNumber("40428769")
                .birthDate(LocalDate.of(1997, 7, 16))
                .build();

        Exception exception = assertThrows(PersonalizeException.class, () -> candidateService.saveCandidate(candidateDtoNew));

        assertEquals("El tipo de documento del candidato es obligatorio", exception.getMessage());
    }

    @Test
    void saveCandidate_WhenDocumentNumberIsEmpty() {
        CandidateDto candidateDtoNew =  CandidateDto.builder()
                .name("Maximiliano")
                .lastName("Monje")
                .documentType("DNI")
                .documentNumber("")
                .birthDate(LocalDate.of(1997, 7, 16))
                .build();

        Exception exception = assertThrows(PersonalizeException.class, () -> candidateService.saveCandidate(candidateDtoNew));

        assertEquals("El número de documento del candidato es obligatorio", exception.getMessage());
    }

    @Test
    void saveCandidate_WhenDocumentTypeDocumentIsInvalid() {
        CandidateDto candidateDtoNew = CandidateDto.builder()
                .name("Maximiliano")
                .lastName("Monje")
                .documentType("fff")
                .documentNumber("40428769")
                .birthDate(LocalDate.of(1997, 7, 16))
                .build();

        Exception exception = assertThrows(PersonalizeException.class, () -> candidateService.saveCandidate(candidateDtoNew));

        assertEquals("El tipo de documento ingresado no es válido", exception.getMessage());
    }

    @Test
    void saveCandidate_WhenBirthDateIsEmpty() {
        CandidateDto candidateDtoNew =  CandidateDto.builder()
                .name("Maximiliano")
                .lastName("Monje")
                .documentType("DNI")
                .documentNumber("40428769")
                .build();

        Exception exception = assertThrows(PersonalizeException.class, () -> candidateService.saveCandidate(candidateDtoNew));

        assertEquals("La fecha de nacimiento del candidato es obligatoria", exception.getMessage());
    }

    @Test
    void saveCandidate_WhenBirthDateIsInvalid() {
        CandidateDto candidateDtoNew = CandidateDto.builder()
                .name("Maximiliano")
                .lastName("Monje")
                .documentType("DNI")
                .documentNumber("40428769")
                .birthDate(LocalDate.now())
                .build();

        Exception exception = assertThrows(PersonalizeException.class, () -> candidateService.saveCandidate(candidateDtoNew));

        assertEquals("La fecha de nacimiento ingresada no es válida. Debe tener entre 16 y 100 años.", exception.getMessage());
    }

    @Test
    void saveCandidate_WithDuplicatedDocument() {

        CandidateDto candidateDtoNew = CandidateDto.builder()
                .name("Maximiliano")
                .lastName("Monje")
                .documentType("DNI")
                .documentNumber("40428769")
                .birthDate(LocalDate.of(1997, 7, 16))
                .build();

        when(candidateRepository.existsByDocumentNumber(candidateDtoNew.getDocumentNumber())).thenReturn(true);
        when(candidateRepository.existsByDocumentType(candidateDtoNew.getDocumentType())).thenReturn(true);

        assertThrows(PersonalizeException.class, () -> candidateService.saveCandidate(candidateDtoNew));

        verify(candidateRepository, never()).save(any(Candidate.class));
    }

    @Test
    void updateCandidate_WhenValidData() {


        CandidateDto candidateDtoNew = new CandidateDto("NuevoNombre", "NuevoApellido", "DNI", "87654321", LocalDate.of(1995, 5, 15));

        when(candidateRepository.findById(candidate.getId())).thenReturn(Optional.of(candidate));
        when(candidateRepository.save(any(Candidate.class))).thenReturn(candidate);

        candidateService.updateCandidate(candidate.getId(), candidateDtoNew);

        assertEquals("NuevoNombre", candidate.getName());
        assertEquals("NuevoApellido", candidate.getLastName());
        assertEquals("DNI", candidate.getDocumentType());
        assertEquals("87654321", candidate.getDocumentNumber());
        assertEquals(LocalDate.of(1995, 5, 15), candidate.getBirthDate());
        verify(candidateRepository).save(candidate);
    }


    @Test
    void updateCandidate_WhenCandidateNotFound() {
        Long id = 2L;
        when(candidateRepository.findById(id)).thenReturn(Optional.empty());

        Exception exception = assertThrows(EntityNotFoundException.class, () -> candidateService.updateCandidate(id, candidateDto));

        assertEquals("No se encontró un candidato con ese ID", exception.getMessage());
        verify(candidateRepository, never()).save(any(Candidate.class));
    }

    @Test
    void addTechnology_WhenValidData() {

        CandidateTechnology candidateTechnologyYears = new CandidateTechnology();
        candidateTechnologyYears.setYearsOfExperience(5);
        Technology technology = new Technology();
        technology.setId(1L);

        candidate.setTechnologies(new ArrayList<>());

        when(technologyRepository.findById(candidate.getId())).thenReturn(Optional.of(technology));
        when(candidateRepository.findById(candidate.getId())).thenReturn(Optional.of(candidate));
        when(candidateTechnologyRepository.existsByCandidateAndTechnology(candidate, technology)).thenReturn(false);

        candidateService.addTechnology(candidate.getId(), technology.getId(), candidateTechnologyYears);

        verify(candidateTechnologyRepository).save(any(CandidateTechnology.class));

    }

    @Test
    void addTechnology_WhenYearsOfExperienceIsNegative() {
        Long technologyId = 1L;
        Long candidateId = 1L;

        CandidateTechnology candidateTechnologyYears = new CandidateTechnology();
        candidateTechnologyYears.setYearsOfExperience(-1);

        when(technologyRepository.findById(technologyId)).thenReturn(Optional.of(new Technology()));
        when(candidateRepository.findById(candidate.getId())).thenReturn(Optional.of(new Candidate()));

        assertThrows(PersonalizeException.class, () -> candidateService.addTechnology(candidateId, technologyId, candidateTechnologyYears));
    }

    @Test
    void addTechnology_WhenTechnologyNotFound() {
        Long technologyId = 1L;
        Long candidateId = 1L;
        CandidateTechnology candidateTechnologyYears = new CandidateTechnology();

        when(technologyRepository.findById(technologyId)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> candidateService.addTechnology(candidateId, technologyId, candidateTechnologyYears));
    }

    @Test
    void addTechnology_WhenCandidateNotFound() {
        Long technologyId = 1L;
        Long candidateId = 1L;
        CandidateTechnology candidateTechnologyYears = new CandidateTechnology();

        when(technologyRepository.findById(technologyId)).thenReturn(Optional.of(new Technology()));
        when(candidateRepository.findById(candidate.getId())).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> candidateService.addTechnology(candidateId , technologyId, candidateTechnologyYears));
    }

    @Test
    void addCandidate_WhenCombinationExists() {
        Long technologyId = 1L;
        Long candidateId = 1L;
        CandidateTechnology candidateTechnologyYears = new CandidateTechnology();
        Technology technology = new Technology();


        when(technologyRepository.findById(candidate.getId())).thenReturn(Optional.of(technology));
        when(candidateRepository.findById(candidate.getId())).thenReturn(Optional.of(candidate));
        when(candidateTechnologyRepository.existsByCandidateAndTechnology(candidate, technology)).thenReturn(true);

        assertThrows(PersonalizeException.class, () -> candidateService.addTechnology(candidateId, technologyId, candidateTechnologyYears));
    }


    @Test
    void getTechnologiesAndExperience_WhenFound() {
        List<TechnologyExperienceProjection> projections = new ArrayList<>();
        TechnologyExperienceProjection projection = mock(TechnologyExperienceProjection.class);


        projections.add(projection);

        when(candidateTechnologyRepository.findTechnologiesAndExperience(candidate.getId())).thenReturn(projections);
        when(projection.getTechnology()).thenReturn(technology);
        when(projection.getYearsOfExperience()).thenReturn(5);
        when(technologyMapper.technologyToTechnologyDto(technology)).thenReturn(technologyDto);

        List<TechnologiesAndYearsExperienceDto> result = candidateService.getTechnologiesAndExperience(candidate.getId());

        assertNotNull(result);
        assertEquals(1, result.size());
        assertEquals("Java", result.get(0).getTechnologyDto().getName());
        assertEquals("11", result.get(0).getTechnologyDto().getVersion());
        assertEquals(5, result.get(0).getYearsExperience());

        verify(candidateTechnologyRepository).findTechnologiesAndExperience(candidate.getId());
        verify(technologyMapper).technologyToTechnologyDto(technology);


    }

    @Test
    void getTechnologiesAndExperience_WhenNotFound() {
        Long candidateId = 2L;

        when(candidateTechnologyRepository.findTechnologiesAndExperience(candidateId)).thenReturn(new ArrayList<>());

        assertThrows(EntityNotFoundException.class, () -> candidateService.getTechnologiesAndExperience(candidateId));
    }


}








