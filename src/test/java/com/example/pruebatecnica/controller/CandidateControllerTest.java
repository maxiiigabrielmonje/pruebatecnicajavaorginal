package com.example.pruebatecnica.controller;

import com.example.pruebatecnica.dto.*;
import com.example.pruebatecnica.exception.PersonalizeException;
import com.example.pruebatecnica.model.Candidate;
import com.example.pruebatecnica.model.CandidateTechnology;
import com.example.pruebatecnica.model.Technology;
import com.example.pruebatecnica.service.service_imp.CandidateServiceImp;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.persistence.EntityNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.BDDMockito.*;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;


@WebMvcTest(controllers = CandidateController.class)
@AutoConfigureMockMvc(addFilters = false)
@ExtendWith(MockitoExtension.class)
 class CandidateControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CandidateServiceImp candidateService;

    @Autowired
    private ObjectMapper objectMapper;

    private Candidate candidate;

    private CandidateDto candidateDto;

    @BeforeEach
    public void init() {
        candidateDto = CandidateDto.builder()
                .name("Maximiliano")
                .lastName("Monje")
                .documentType("DNI")
                .documentNumber("40428769")
                .birthDate(LocalDate.of(1997, 7, 16))
                .build();

        candidate = Candidate.builder()
                .id(1L)
                .name("Maximiliano")
                .lastName("Monje")
                .documentType("DNI")
                .documentNumber("40428769")
                .birthDate(LocalDate.of(1997, 7, 16))
                .build();
    }

    @Test
    void getCandidate_WhenExists() throws Exception {
        Long candidateId = 1L;
        given(candidateService.getCandidate(candidate.getId())).willReturn(candidateDto);
        ResultActions response = mockMvc.perform(MockMvcRequestBuilders.get("/api/candidate/{id}", candidateId)
                .contentType(MediaType.APPLICATION_JSON));

        response.andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(candidateDto.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lastName").value(candidateDto.getLastName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.documentType").value(candidateDto.getDocumentType()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.documentNumber").value(candidateDto.getDocumentNumber()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.birthDate[0]").value("1997"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.birthDate[1]").value("7"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.birthDate[2]").value("16"));

    }

    @Test
    void getCandidate_WhenNotExists() throws Exception {
        Long candidateId = 2L;
        given(candidateService.getCandidate(candidateId))
                .willThrow(new EntityNotFoundException("No se encontró el candidato con el id: " + candidateId));

        ResultActions response = mockMvc.perform(MockMvcRequestBuilders.get("/api/candidate/{id}", candidateId)
                .contentType(MediaType.APPLICATION_JSON));

        response.andExpect(MockMvcResultMatchers.status().isNotFound())
                .andExpect(MockMvcResultMatchers.content().string("No se encontró el candidato con el id: " + candidateId));
    }

    @Test
    void deleteCandidate_WhenExists() throws Exception {
        Long candidateId = 1L;
        doNothing().when(candidateService).deleteCandidate(candidateId);

        ResultActions response = mockMvc.perform(MockMvcRequestBuilders.delete("/api/candidate/{id}", candidateId));

        response.andExpect(MockMvcResultMatchers.status().isNoContent());
    }

    @Test
    void deleteCandidate_WhenNotExists() throws Exception {
        Long candidateId = 2L;
        doThrow(new EntityNotFoundException("No se encontró el candidato con el id: " + candidateId))
                .when(candidateService).deleteCandidate(candidateId);

        ResultActions response = mockMvc.perform(MockMvcRequestBuilders.delete("/api/candidate/{id}", candidateId));

        response.andExpect(MockMvcResultMatchers.status().isNotFound())
                .andExpect(MockMvcResultMatchers.content().string("No se encontró el candidato con el id: " + candidateId));
    }

    @Test
    void getAllCandidates_WhenExist() throws Exception{
        List<CandidateDto> candidatesDtos = new ArrayList<>();
        candidatesDtos.add(candidateDto);
        candidatesDtos.add(CandidateDto.builder()
                .name("Pedro")
                .lastName("Pascal")
                .documentType("LC")
                .documentNumber("40333769")
                .birthDate(LocalDate.of(1992, 2, 26))
                .build());

        given(candidateService.getAllCandidates()).willReturn(candidatesDtos);

        ResultActions response = mockMvc.perform(MockMvcRequestBuilders.get("/api/candidate")
                .contentType(MediaType.APPLICATION_JSON));

        response.andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(2)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value("Maximiliano"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].lastName").value("Monje"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].documentType").value("DNI"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].documentNumber").value("40428769"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].birthDate[0]").value("1997"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].birthDate[1]").value("7"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].birthDate[2]").value("16"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].name").value("Pedro"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].lastName").value("Pascal"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].documentType").value("LC"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].documentNumber").value("40333769"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].birthDate[0]").value("1992"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].birthDate[1]").value("2"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].birthDate[2]").value("26"));


    }

    @Test
    void getAllCandidates_WhenNoExist() throws Exception {
        given(candidateService.getAllCandidates())
                .willThrow(new EntityNotFoundException("No se encontraron candidatos"));

        ResultActions response = mockMvc.perform(MockMvcRequestBuilders.get("/api/candidate")
                .contentType(MediaType.APPLICATION_JSON));

        response.andExpect(MockMvcResultMatchers.status().isNotFound())
                .andExpect(MockMvcResultMatchers.content().string("No se encontraron candidatos"));
    }


    @Test
    void createCandidate_Success() throws Exception {

        doNothing().when(candidateService).saveCandidate(candidateDto);

        ResultActions response = mockMvc.perform(MockMvcRequestBuilders.post("/api/candidate")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(candidateDto)));

        response.andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.content().string("Candidato Creado y Guardado con éxito"));
    }

    @Test
    void updateCandidate_Success() throws Exception {

        CandidateDto updatedCandidateDto = CandidateDto.builder()
                .name("Nuevo")
                .lastName("New")
                .documentType("LC")
                .documentNumber("20328769")
                .birthDate(LocalDate.of(1977, 7, 11))
                .build();

        doNothing().when(candidateService).updateCandidate(candidate.getId(),updatedCandidateDto);

        ResultActions response = mockMvc.perform(MockMvcRequestBuilders.put("/api/candidate/{id}", candidate.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updatedCandidateDto)));

        response.andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("Candidato Actualizado con éxito"));
    }

    @Test
    void addTechnology_Success() throws Exception {
        Long technologyId = 1L;
        CandidateTechnology candidateTechnology = new CandidateTechnology();
        candidateTechnology.setYearsOfExperience(2);

        doNothing().when(candidateService).addTechnology(candidate.getId(), technologyId, candidateTechnology);

        ResultActions response = mockMvc.perform(MockMvcRequestBuilders.put("/api/candidate/{candidateId}/{technologyId}", candidate.getId(), technologyId)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(candidateTechnology)));

        response.andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("Agregada la tecnología al candidato con éxito"));
    }

    @Test
    void completeCandidate_Success() throws Exception {

        List<TechnologiesAndYearsExperienceDto> technologiesAndYearsExperienceDto = new ArrayList<>();

        given(candidateService.getCandidate(candidate.getId())).willReturn(candidateDto);
        given(candidateService.getTechnologiesAndExperience(candidate.getId())).willReturn(technologiesAndYearsExperienceDto);

        CandidateCompleteDto candidateCompleteDto = new CandidateCompleteDto();
        candidateCompleteDto.setCandidateDto(candidateDto);
        candidateCompleteDto.setTechnologiesWithExperience(technologiesAndYearsExperienceDto);

        ResultActions response = mockMvc.perform(MockMvcRequestBuilders.get("/api/candidate/complete/{id}", candidate.getId())
                .contentType(MediaType.APPLICATION_JSON));

        response.andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.candidateDto.name").value(candidateDto.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.candidateDto.lastName").value(candidateDto.getLastName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.candidateDto.documentType").value(candidateDto.getDocumentType()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.candidateDto.documentNumber").value(candidateDto.getDocumentNumber()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.candidateDto.birthDate[0]").value("1997"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.candidateDto.birthDate[1]").value("7"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.candidateDto.birthDate[2]").value("16"));


    }




}
