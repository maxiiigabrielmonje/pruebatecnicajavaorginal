package com.example.pruebatecnica.controller;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;

import com.example.pruebatecnica.dto.CandidatesAndYearsExperienceDto;
import com.example.pruebatecnica.dto.TechnologyCompleteDto;
import com.example.pruebatecnica.dto.TechnologyDto;
import com.example.pruebatecnica.exception.PersonalizeException;
import com.example.pruebatecnica.model.CandidateTechnology;
import com.example.pruebatecnica.model.Technology;
import com.example.pruebatecnica.service.service_imp.TechnologyServiceImp;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.persistence.EntityNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


@WebMvcTest(controllers = TechnologyController.class)
@AutoConfigureMockMvc(addFilters = false)
@ExtendWith(MockitoExtension.class)
class TechnologyControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TechnologyServiceImp technologyService;

    @Autowired
    private ObjectMapper objectMapper;

    private TechnologyDto technologyDto;

    private Technology technology;

    @BeforeEach
    public void init() {
        technologyDto = TechnologyDto.builder()
                .name("Java")
                .version("11")
                .build();

        technology = Technology.builder()
                .id(1L)
                .name("Java")
                .version("11")
                .build();

    }

    @Test
     void getTechnology_WhenExists() throws Exception {
        Long technologyId = 1L;
        given(technologyService.getTechnology(technology.getId())).willReturn(technologyDto);
        ResultActions response = mockMvc.perform(MockMvcRequestBuilders.get("/api/tech/{id}", technologyId)
                .contentType(MediaType.APPLICATION_JSON));

        response.andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(technologyDto.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.version").value(technologyDto.getVersion()));
    }

    @Test
     void getTechnology_WhenNotExists() throws Exception {
        Long technologyId = 2L;
        given(technologyService.getTechnology(technologyId))
                .willThrow(new EntityNotFoundException("No se encontró la tecnología con el id: " + technologyId));

        ResultActions response = mockMvc.perform(MockMvcRequestBuilders.get("/api/tech/{id}", technologyId)
                .contentType(MediaType.APPLICATION_JSON));

        response.andExpect(MockMvcResultMatchers.status().isNotFound())
                .andExpect(MockMvcResultMatchers.content().string("No se encontró la tecnología con el id: " + technologyId));
    }

    @Test
    void deleteTechnology_WhenExists() throws Exception {
        Long technologyId = 1L;
        doNothing().when(technologyService).deleteTechnology(technologyId);

        ResultActions response = mockMvc.perform(MockMvcRequestBuilders.delete("/api/tech/{id}", technologyId));

        response.andExpect(MockMvcResultMatchers.status().isNoContent());
    }

    @Test
    void deleteTechnology_WhenNotExists() throws Exception {
        Long technologyId = 2L;
        doThrow(new EntityNotFoundException("No se encontró la tecnología con el id: " + technologyId))
                .when(technologyService).deleteTechnology(technologyId);

        ResultActions response = mockMvc.perform(MockMvcRequestBuilders.delete("/api/tech/{id}", technologyId));

        response.andExpect(MockMvcResultMatchers.status().isNotFound())
                .andExpect(MockMvcResultMatchers.content().string("No se encontró la tecnología con el id: " + technologyId));
    }

    @Test
    void getAllTechnologies_WhenExist() throws Exception {
        List<TechnologyDto> technologiesDto = new ArrayList<>();
        technologiesDto.add(technologyDto);
        technologiesDto.add(new TechnologyDto("Python", "3.8"));

        given(technologyService.getAllTechnologies()).willReturn(technologiesDto);

        ResultActions response = mockMvc.perform(MockMvcRequestBuilders.get("/api/tech")
                .contentType(MediaType.APPLICATION_JSON));

        response.andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(2)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value("Java"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].version").value("11"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].name").value("Python"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].version").value("3.8"));
    }

    @Test
    void getAllTechnologies_WhenNoExist() throws Exception {
        given(technologyService.getAllTechnologies())
                .willThrow(new EntityNotFoundException("No se encontraron tecnologías"));

        ResultActions response = mockMvc.perform(MockMvcRequestBuilders.get("/api/tech")
                .contentType(MediaType.APPLICATION_JSON));

        response.andExpect(MockMvcResultMatchers.status().isNotFound())
                .andExpect(MockMvcResultMatchers.content().string("No se encontraron tecnologías"));
    }

    @Test
    void createTechnology_Success() throws Exception {

        doNothing().when(technologyService).saveTechnology(technologyDto);

        ResultActions response = mockMvc.perform(MockMvcRequestBuilders.post("/api/tech")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(technologyDto)));

        response.andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.content().string("Tecnología creada con éxito"));
    }

    @Test
    void updateTechnology_Success() throws Exception {

        TechnologyDto updatedTechnologyDto = new TechnologyDto("Java", "12");
        doNothing().when(technologyService).updateTechnology(technology.getId(),updatedTechnologyDto);

        ResultActions response = mockMvc.perform(MockMvcRequestBuilders.put("/api/tech/{id}", technology.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updatedTechnologyDto)));

        response.andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("Tecnología actualizada con éxito"));
    }

    @Test
    void addCandidate_Success() throws Exception {
        Long candidateId = 1L;
        CandidateTechnology candidateTechnology = new CandidateTechnology();
                    candidateTechnology.setYearsOfExperience(2);

        doNothing().when(technologyService).addCandidate(technology.getId(), candidateId, candidateTechnology);

        ResultActions response = mockMvc.perform(MockMvcRequestBuilders.put("/api/tech/{technologyId}/{candidateId}", technology.getId(), candidateId)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(candidateTechnology)));

        response.andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("Agregado el candidato a la tecnología con éxito"));
    }


    @Test
    void completeTechnology_Success() throws Exception {

        List<CandidatesAndYearsExperienceDto> candidatesAndYearsExperienceDto = new ArrayList<>();

        given(technologyService.getTechnology(technology.getId())).willReturn(technologyDto);
        given(technologyService.getCandidatesAndExperience(technology.getId())).willReturn(candidatesAndYearsExperienceDto);

        TechnologyCompleteDto technologyCompleteDto = new TechnologyCompleteDto();
        technologyCompleteDto.setTechnologyDto(technologyDto);
        technologyCompleteDto.setCandidatesWithExperience(candidatesAndYearsExperienceDto);

        ResultActions response = mockMvc.perform(MockMvcRequestBuilders.get("/api/tech/complete/{id}", technology.getId())
                .contentType(MediaType.APPLICATION_JSON));

        response.andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.technologyDto.name").value(technologyDto.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.technologyDto.version").value(technologyDto.getVersion()));


    }

    @Test
    void completeTechnologyNV_Success() throws Exception {

        List<CandidatesAndYearsExperienceDto> candidatesAndYearsExperienceDto = new ArrayList<>();

        given(technologyService.getTechnologyNameAndVersion(technologyDto.getName(), technologyDto.getVersion())).willReturn(technologyDto);
        given(technologyService.getCandidatesAndExperienceNV(technologyDto)).willReturn(candidatesAndYearsExperienceDto);

        TechnologyCompleteDto technologyCompleteDto = new TechnologyCompleteDto();
        technologyCompleteDto.setTechnologyDto(technologyDto);
        technologyCompleteDto.setCandidatesWithExperience(candidatesAndYearsExperienceDto);

        ResultActions response = mockMvc.perform(MockMvcRequestBuilders.get("/api/tech/complete")
                .param("name", technologyDto.getName())
                .param("version", technologyDto.getVersion())
                .contentType(MediaType.APPLICATION_JSON));

        response.andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.technologyDto.name").value(technologyDto.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.technologyDto.version").value(technologyDto.getVersion()));

    }


}

