package com.example.pruebatecnica.repository;

import com.example.pruebatecnica.dto.CandidateExperienceProjection;
import com.example.pruebatecnica.dto.TechnologyExperienceProjection;
import com.example.pruebatecnica.model.Candidate;
import com.example.pruebatecnica.model.CandidateTechnology;
import com.example.pruebatecnica.model.Technology;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CandidateTechnologyRepository extends JpaRepository<CandidateTechnology, Long> {

    Boolean existsByCandidateAndTechnology(Candidate candidate, Technology technology);


    @Query("SELECT ct.candidate as candidate, ct.yearsOfExperience as yearsOfExperience FROM CandidateTechnology ct WHERE ct.technology.id = :technologyId")
    List<CandidateExperienceProjection> findCandidatesAndExperience(@Param("technologyId") Long technologyId);


    @Query("SELECT ct.technology as technology, ct.yearsOfExperience as yearsOfExperience FROM CandidateTechnology ct WHERE ct.candidate.id = :candidateId")
    List<TechnologyExperienceProjection> findTechnologiesAndExperience(@Param("candidateId") Long candidateId);


    @Query("SELECT ct.candidate as candidate, ct.yearsOfExperience as yearsOfExperience FROM CandidateTechnology ct WHERE ct.technology.name = :name AND ct.technology.version = :version")
    List<CandidateExperienceProjection> findCandidatesAndExperienceNV(@Param("name") String name, @Param("version") String version);


}
