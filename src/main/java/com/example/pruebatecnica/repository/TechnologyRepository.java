package com.example.pruebatecnica.repository;

import com.example.pruebatecnica.model.Technology;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TechnologyRepository extends JpaRepository<Technology,Long> {

    Boolean existsByNameAndVersion(String name, String version);

    Optional<Technology> findByNameAndVersion(String name, String version);
}
