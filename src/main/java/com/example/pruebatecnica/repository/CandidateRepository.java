package com.example.pruebatecnica.repository;

import com.example.pruebatecnica.model.Candidate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CandidateRepository  extends JpaRepository<Candidate, Long> {

    Boolean existsByDocumentNumber(String documentNumber);

    Boolean existsByDocumentType(String documentType);

    Boolean existsByDocumentNumberAndDocumentType(String documentNumber, String documentType);
}
