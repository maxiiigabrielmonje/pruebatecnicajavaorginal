package com.example.pruebatecnica.mapper;


import com.example.pruebatecnica.dto.CandidateDto;
import com.example.pruebatecnica.model.Candidate;
import org.springframework.stereotype.Component;

@Component
public class CandidateMapper {

    public Candidate candidateDtoToCandidate(Long id, CandidateDto candidateDto){
        return Candidate.builder()
             .id(id)
             .name(candidateDto.getName())
                .lastName(candidateDto.getLastName())
                .documentType(candidateDto.getDocumentType())
                .documentNumber(candidateDto.getDocumentNumber())
                .birthDate(candidateDto.getBirthDate())
             .build();
    }

    public CandidateDto candidateToCandidateDto(Candidate candidate){
        return CandidateDto.builder()
              .name(candidate.getName())
              .lastName(candidate.getLastName())
              .documentType(candidate.getDocumentType())
              .documentNumber(candidate.getDocumentNumber())
              .birthDate(candidate.getBirthDate())
              .build();
    }
}
