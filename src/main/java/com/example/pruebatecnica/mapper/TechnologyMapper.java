package com.example.pruebatecnica.mapper;


import com.example.pruebatecnica.dto.TechnologyDto;
import com.example.pruebatecnica.model.Technology;
import org.springframework.stereotype.Component;

@Component
public class TechnologyMapper {

    public Technology technologyDtoToTechnology(Long id, TechnologyDto technologyDto){
        return Technology.builder()
                .id(id)
                .name(technologyDto.getName())
                .version(technologyDto.getVersion())
                .build();
    }

    public TechnologyDto technologyToTechnologyDto(Technology technology){
        return TechnologyDto.builder()
                .name(technology.getName())
                .version(technology.getVersion())
                .build();
    }
}
