package com.example.pruebatecnica.model;


import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.*;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "candidates")
public class Candidate {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @NotBlank
    @Column(nullable = false)
    private String name;

    @NotNull
    @NotBlank
    @Column(nullable = false, name="last_name")
    private String lastName;

    @NotNull
    @NotBlank
    @Column(nullable = false, name="document_type")
    private String documentType;

    @NotNull
    @NotBlank
    @Column(nullable = false, name="document_number")
    private String documentNumber;

    @NotNull
    @Column(nullable = false, name="birth_date")
    private LocalDate birthDate;

    @CreatedDate
    private LocalDateTime dateCreated;

    @LastModifiedDate
    private LocalDateTime dateModified;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "candidate")
    private List<CandidateTechnology> technologies;
}
