package com.example.pruebatecnica.controller;

import com.example.pruebatecnica.dto.CandidatesAndYearsExperienceDto;
import com.example.pruebatecnica.dto.TechnologyCompleteDto;
import com.example.pruebatecnica.dto.TechnologyDto;
import com.example.pruebatecnica.model.CandidateTechnology;
import com.example.pruebatecnica.service.TechnologyService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequiredArgsConstructor
@RestController
@Tag(name="Technology")
@RequestMapping("/api/tech")
public class TechnologyController {

    private final TechnologyService technologyServiceImp;

    @Operation(
            description = "Get endpoint que trae una tecnología por su ID",
            summary = "Obtiene una tecnología",
            responses = {
                    @ApiResponse(
                            description = "Success",
                            responseCode = "200"
                    )
            },
            parameters = {
                    @Parameter(
                            name = "id",
                            description = "ID de la tecnología",
                            required = true
                    )
            }
    )
    @GetMapping("/{id}")
    public ResponseEntity<TechnologyDto> getTechnology(@PathVariable Long id){
        TechnologyDto technologyDto = technologyServiceImp.getTechnology(id);
        return new ResponseEntity<>(technologyDto, HttpStatus.OK);
    }


    @Operation(
            description = "Get endpoint que trae todas las tecnologías",
            summary = "Obtiene todas las tecnologías",
            responses = {
                    @ApiResponse(
                            description = "Success",
                            responseCode = "200"
                    )
            }
    )
    @GetMapping
    public ResponseEntity<List<TechnologyDto>> getAllTechnologies(){
            List<TechnologyDto> technologiesDto = technologyServiceImp.getAllTechnologies();
            return new ResponseEntity<>(technologiesDto, HttpStatus.OK);

    }

    @Operation(
            description = "Delete endpoint que elimina una tecnología por su ID",
            summary = "Elimina una tecnología",
            responses = {
                    @ApiResponse(
                            description = "Success",
                            responseCode = "204"
                    )
            },
            parameters = {
                    @Parameter(
                            name = "id",
                            description = "ID de la tecnología a eliminar",
                            required = true
                    )
            }
    )
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteTechnology(@PathVariable Long id){
            technologyServiceImp.deleteTechnology(id);
            return new ResponseEntity<>( HttpStatus.NO_CONTENT);
    }

    @Operation(
            description = "Post endpoint que crea una tecnología",
            summary = "Crea una tecnología",
            responses = {
                    @ApiResponse(
                            description = "Created",
                            responseCode = "201"
                    )
            }
    )
    @PostMapping
    public ResponseEntity<String> createTechnology(@Valid @RequestBody TechnologyDto technologyDto){
            technologyServiceImp.saveTechnology(technologyDto);
            return new ResponseEntity<>("Tecnología creada con éxito", HttpStatus.CREATED);

    }


    @Operation(
            description = "Put endpoint que actualiza una tecnología",
            summary = "Actualiza una tecnología",
            responses = {
                    @ApiResponse(
                            description = "Success",
                            responseCode = "200"
                    )
            },
            parameters = {
                    @Parameter(
                            name = "id",
                            description = "ID de la tecnología a actualizar",
                            required = true
                    )
            }
    )
    @PutMapping("/{id}")
    public ResponseEntity<String> updateTechnology(@PathVariable Long id, @Valid @RequestBody TechnologyDto technologyDto){
            technologyServiceImp.updateTechnology(id, technologyDto);
            return new ResponseEntity<>("Tecnología actualizada con éxito", HttpStatus.OK);
    }

    @Operation(
            description = "Put endpoint que agrega un candidato a una tecnología",
            summary = "Agrega un candidato",
            responses = {
                    @ApiResponse(
                            description = "Success",
                            responseCode = "200"
                    )
            },
            parameters = {
                    @Parameter(
                            name = "technology_id",
                            description = "ID de la tecnología que quieras relacionar con algún candidato",
                            required = true
                    ),
                    @Parameter(
                            name = "candidate_id",
                            description = "ID del candidato que quieras relacionar con alguna tecnología",
                            required = true
                    )
            }
    )
    @PutMapping("/{technologyId}/{candidateId}")
    public ResponseEntity<String>addCandidate(@PathVariable Long technologyId, @PathVariable Long candidateId,@Valid @RequestBody CandidateTechnology candidateTechnology){
            technologyServiceImp.addCandidate(technologyId, candidateId, candidateTechnology);
            return new ResponseEntity<>("Agregado el candidato a la tecnología con éxito", HttpStatus.OK);

    }


    @Operation(
            description = "Get endpoint que trae una tecnología con sus candidatos y años de exp por su ID",
            summary = "Obtiene una tecnología completa",
            responses = {
                    @ApiResponse(
                            description = "Success",
                            responseCode = "200"
                    )
            },
            parameters = {
                    @Parameter(
                            name = "id",
                            description = "ID de la tecnología que quieras ver completa",
                            required = true
                    )
            }
    )
    @GetMapping("/complete/{id}")
    public ResponseEntity<TechnologyCompleteDto>completeTechnology(@PathVariable Long id){
            TechnologyDto technologyDto = technologyServiceImp.getTechnology(id);
            List<CandidatesAndYearsExperienceDto> candidatesAndYearsExperienceDto = technologyServiceImp.getCandidatesAndExperience(id);

            TechnologyCompleteDto technologyCompleteDto = new TechnologyCompleteDto();
            technologyCompleteDto.setTechnologyDto(technologyDto);
            technologyCompleteDto.setCandidatesWithExperience(candidatesAndYearsExperienceDto);

            return new ResponseEntity<>(technologyCompleteDto, HttpStatus.OK);

    }


    @Operation(
            description = "GET endpoint que trae una tecnología con sus candidatos y años de exp por su nombre y versión",
            summary = "Obtiene una tecnología completa",
            responses = {
                    @ApiResponse(
                            description = "Success",
                            responseCode = "200"
                    )
            },
            parameters = {
                    @Parameter(
                            name = "name",
                            description = "nombre de la tecnología que quieras ver completa",
                            required = true
                    ),
                    @Parameter(
                            name = "version",
                            description = "version del candidato que quieras ver completa",
                            required = true
                    )
            }
    )
    @GetMapping("/complete")
    public ResponseEntity<TechnologyCompleteDto> completeTechnologyNameAndVersion(@RequestParam String name, @RequestParam String version) {
        TechnologyDto technologyDto = technologyServiceImp.getTechnologyNameAndVersion(name, version);
        List<CandidatesAndYearsExperienceDto> candidatesAndYearsExperienceDto = technologyServiceImp.getCandidatesAndExperienceNV(technologyDto);

        TechnologyCompleteDto technologyCompleteDto = new TechnologyCompleteDto();
        technologyCompleteDto.setTechnologyDto(technologyDto);
        technologyCompleteDto.setCandidatesWithExperience(candidatesAndYearsExperienceDto);

        return new ResponseEntity<>(technologyCompleteDto, HttpStatus.OK);
    }

}
