package com.example.pruebatecnica.controller;

import com.example.pruebatecnica.dto.CandidateCompleteDto;
import com.example.pruebatecnica.dto.CandidateDto;
import com.example.pruebatecnica.dto.TechnologiesAndYearsExperienceDto;
import com.example.pruebatecnica.model.CandidateTechnology;
import com.example.pruebatecnica.service.CandidateService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequiredArgsConstructor
@RestController
@Tag(name="Candidate")
@RequestMapping("/api/candidate")
public class CandidateController {

    private final CandidateService candidateServiceImp;

    @Operation(
            description = "Get endpoint que trae un candidato por su ID",
            summary = "Obtiene un candidato",
            responses = {
                    @ApiResponse(
                            description = "Success",
                            responseCode = "200"
                    )
            },
            parameters = {
                    @Parameter(
                            name = "id",
                            description = "ID del candidato",
                            required = true
                    )
            }
    )
    @GetMapping("/{id}")
    public ResponseEntity<CandidateDto> getCandidate(@PathVariable Long id){
            CandidateDto candidateDto = candidateServiceImp.getCandidate(id);
            return  new ResponseEntity<>(candidateDto, HttpStatus.OK);
    }

    @Operation(
            description = "Get endpoint que trae todos los candidatos",
            summary = "Obtiene todos los candidatos",
            responses = {
                    @ApiResponse(
                            description = "Success",
                            responseCode = "200"
                    )
            }
    )
    @GetMapping
    public ResponseEntity< List<CandidateDto>> getAllCandidates(){
            List<CandidateDto> candidatesDto = candidateServiceImp.getAllCandidates();
            return new ResponseEntity<>(candidatesDto, HttpStatus.OK);
    }

    @Operation(
            description = "Delete endpoint que elimina un candidato por su ID",
            summary = "Elimina un candidato",
            responses = {
                    @ApiResponse(
                            description = "Success",
                            responseCode = "204"
                    )
            },
            parameters = {
                    @Parameter(
                            name = "id",
                            description = "ID del candidato a eliminar",
                            required = true
                    )
            }
    )
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteCandidate(@PathVariable Long id){
            candidateServiceImp.deleteCandidate(id);
            return  new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Operation(
            description = "Post endpoint que crea un candidato",
            summary = "Crea un candidato",
            responses = {
                    @ApiResponse(
                            description = "Created",
                            responseCode = "201"
                    )
            }
    )
    @PostMapping
    public ResponseEntity<String> createCandidate(@Valid @RequestBody CandidateDto candidateDto){
            candidateServiceImp.saveCandidate(candidateDto);
            return  new ResponseEntity<>("Candidato Creado y Guardado con éxito",HttpStatus.CREATED);
    }

    @Operation(
            description = "Put endpoint que actualiza un candidato",
            summary = "Actualiza un candidato",
            responses = {
                    @ApiResponse(
                            description = "Success",
                            responseCode = "200"
                    )
            },
            parameters = {
                    @Parameter(
                            name = "id",
                            description = "ID del candidato a actualizar",
                            required = true
                    )
            }
    )
    @PutMapping("/{id}")
    public ResponseEntity<String> updateCandidate(@PathVariable Long id, @Valid @RequestBody CandidateDto candidateDto){
            candidateServiceImp.updateCandidate(id, candidateDto);
            return  new ResponseEntity<>("Candidato Actualizado con éxito",HttpStatus.OK);
    }

    @Operation(
            description = "Put endpoint que agrega una tecnología a un candidato",
            summary = "Agrega una tecnología",
            responses = {
                    @ApiResponse(
                            description = "Success",
                            responseCode = "200"
                    )
            },
            parameters = {
                    @Parameter(
                            name = "candidate_id",
                            description = "ID del candidato que quieras relacionar con alguna tecnología",
                            required = true
                    ),
                    @Parameter(
                            name = "technology_id",
                            description = "ID de la tecnología que quieras relacionar con algún candidato",
                            required = true
                    )
            }
    )
    @PutMapping("/{candidateId}/{technologyId}")
    public ResponseEntity<String>addTechnology(@PathVariable Long candidateId, @PathVariable Long technologyId, @Valid @RequestBody CandidateTechnology candidateTechnology){
            candidateServiceImp.addTechnology(candidateId, technologyId, candidateTechnology);
            return new ResponseEntity<>("Agregada la tecnología al candidato con éxito", HttpStatus.OK);
    }

    @Operation(
            description = "Get endpoint que trae un candidato con sus tecnologías y años de exp por su ID",
            summary = "Obtiene un candidato completo",
            responses = {
                    @ApiResponse(
                            description = "Success",
                            responseCode = "200"
                    )
            },
            parameters = {
                    @Parameter(
                            name = "id",
                            description = "ID del candidato que quieras ver completo",
                            required = true
                    )
            }
    )
    @GetMapping("/complete/{id}")
    public ResponseEntity<CandidateCompleteDto> completeCandidate(@PathVariable Long id){
            CandidateDto candidateDto = candidateServiceImp.getCandidate(id);
            List<TechnologiesAndYearsExperienceDto> technologiesAndYearsExperienceDto = candidateServiceImp.getTechnologiesAndExperience(id);

            CandidateCompleteDto candidateCompleteDto = new CandidateCompleteDto();
            candidateCompleteDto.setCandidateDto(candidateDto);
            candidateCompleteDto.setTechnologiesWithExperience(technologiesAndYearsExperienceDto);

            return new ResponseEntity<>(candidateCompleteDto,HttpStatus.OK);

    }
}
