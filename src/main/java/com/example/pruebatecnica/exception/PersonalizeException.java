package com.example.pruebatecnica.exception;

public class PersonalizeException extends RuntimeException {
    public PersonalizeException(String msg) {
        super(msg);
    }
}
