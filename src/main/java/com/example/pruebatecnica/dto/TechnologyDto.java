package com.example.pruebatecnica.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TechnologyDto {

    @NotNull
    @NotBlank
    private String name;

    @NotNull
    @NotBlank
    private String version;
}
