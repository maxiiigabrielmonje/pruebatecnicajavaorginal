package com.example.pruebatecnica.dto;


import com.example.pruebatecnica.model.Technology;

public interface TechnologyExperienceProjection {
    Technology getTechnology();
    Integer getYearsOfExperience();
}
