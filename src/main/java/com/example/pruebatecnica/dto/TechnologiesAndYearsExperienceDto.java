package com.example.pruebatecnica.dto;

import lombok.*;

@Getter

@AllArgsConstructor

public class TechnologiesAndYearsExperienceDto {

    private TechnologyDto technologyDto;
    private Integer yearsExperience;
}
