package com.example.pruebatecnica.dto;

import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class TechnologyCompleteDto {
    private TechnologyDto technologyDto;
    private List<CandidatesAndYearsExperienceDto> candidatesWithExperience;
}
