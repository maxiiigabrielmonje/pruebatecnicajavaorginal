package com.example.pruebatecnica.dto;

import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class CandidateCompleteDto {
    private CandidateDto candidateDto;
    private List<TechnologiesAndYearsExperienceDto> technologiesWithExperience;
}
