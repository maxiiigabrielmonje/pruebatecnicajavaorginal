package com.example.pruebatecnica.dto;

import com.example.pruebatecnica.model.Candidate;

public interface CandidateExperienceProjection {
    Candidate getCandidate();
    Integer getYearsOfExperience();
}
