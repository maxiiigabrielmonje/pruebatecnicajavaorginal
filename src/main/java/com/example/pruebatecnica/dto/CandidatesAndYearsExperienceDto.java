package com.example.pruebatecnica.dto;

import lombok.*;

@Getter

@AllArgsConstructor

public class CandidatesAndYearsExperienceDto {

    private CandidateDto candidateDto;
    private Integer yearsExperience;
}
