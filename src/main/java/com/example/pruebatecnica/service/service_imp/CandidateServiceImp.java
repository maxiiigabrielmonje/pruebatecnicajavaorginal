package com.example.pruebatecnica.service.service_imp;

import com.example.pruebatecnica.dto.*;
import com.example.pruebatecnica.exception.PersonalizeException;
import com.example.pruebatecnica.mapper.CandidateMapper;
import com.example.pruebatecnica.mapper.TechnologyMapper;
import com.example.pruebatecnica.model.Candidate;
import com.example.pruebatecnica.model.CandidateTechnology;
import com.example.pruebatecnica.model.Technology;
import com.example.pruebatecnica.repository.CandidateRepository;
import com.example.pruebatecnica.repository.CandidateTechnologyRepository;
import com.example.pruebatecnica.repository.TechnologyRepository;
import com.example.pruebatecnica.service.CandidateService;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
@Service
public class CandidateServiceImp implements CandidateService {

    private final CandidateRepository candidateRepository;

    private final TechnologyRepository technologyRepository;

    private final CandidateTechnologyRepository candidateTechnologyRepository;

    private final TechnologyMapper technologyMapper;

    private final CandidateMapper candidateMapper;

    private static final String CANDIDATE_NOT_FOUND_MESSAGE = "No se encontró un candidato con ese ID";

    @Override
    public void saveCandidate(CandidateDto candidateDto) {

        log.info("Validando datos...");
        validateDataCreated(candidateDto);
        Candidate candidate = candidateMapper.candidateDtoToCandidate(null, candidateDto);
        candidate.setDateCreated(LocalDateTime.now());
        candidate.setDateModified(LocalDateTime.now());
        Candidate savedCandidate = candidateRepository.save(candidate);

        log.info("Guardando candidato: {}", savedCandidate.getName());
    }

    @Override
    public CandidateDto getCandidate(Long id) {
        log.info("Obteniendo candidato con ID: {}", id);
        Candidate candidate = candidateRepository.findById(id).orElse(null);

        if(candidate == null){
            throw new EntityNotFoundException(CANDIDATE_NOT_FOUND_MESSAGE);
        }
        log.info("Candidato obtenido con éxito");
        return candidateMapper.candidateToCandidateDto(candidate);

    }

    @Override
    public List<CandidateDto> getAllCandidates() {
        log.info("Obteniendo todos los candidatos");
        List<Candidate> candidates = candidateRepository.findAll();

        if(candidates.isEmpty()){
            throw new EntityNotFoundException("No se encontraron candidatos");
        }

        List<CandidateDto> candidateDtos = new ArrayList<>();

        for (Candidate candidate : candidates) {
            CandidateDto candidateDto = candidateMapper.candidateToCandidateDto(candidate);
            candidateDtos.add(candidateDto);
        }
        log.info("Candidatos obtenidos con éxito");
        return candidateDtos;
    }

    @Override
    public void deleteCandidate(Long id) {
        log.info("Borrando candidato con ID: {}", id);
        if (!candidateRepository.findById(id).isPresent()) {
            throw new EntityNotFoundException(CANDIDATE_NOT_FOUND_MESSAGE);
        }
        candidateRepository.deleteById(id);
        log.info("Candidato borrado con éxito");
    }

    @Override
    public void updateCandidate(Long id, CandidateDto candidateDto) {

        log.info("Actualizando candidato con ID: {}", id);
        Candidate candidateNoValid = candidateRepository.findById(id).orElse(null);

        Candidate candidate = validateDataUpdate(candidateDto, candidateNoValid);

        candidate.setDateModified(LocalDateTime.now());
        candidateRepository.save(candidate);

        log.info("Candidato actualizado con éxito");

    }


    private CandidateDto validateDataCreated(CandidateDto candidateDto) {
        validateDocument(candidateDto);
        validateNameComplete(candidateDto);
        validateBirthDate(candidateDto);
        return candidateDto;
    }



    private void validateDocument(CandidateDto candidateDto) {
        if (candidateRepository.existsByDocumentNumber(candidateDto.getDocumentNumber()) &&
                candidateRepository.existsByDocumentType(candidateDto.getDocumentType())) {
            throw new PersonalizeException("Ya existe un candidato con ese tipo y número de documento");
        }

        if (candidateDto.getDocumentType() == null || candidateDto.getDocumentType().isEmpty()) {
            throw new PersonalizeException("El tipo de documento del candidato es obligatorio");
        } else if (!List.of("DNI", "LE", "LC").contains(candidateDto.getDocumentType().toUpperCase())) {
            throw new PersonalizeException("El tipo de documento ingresado no es válido");
        }

        if (candidateDto.getDocumentNumber() == null || candidateDto.getDocumentNumber().isEmpty()) {
            throw new PersonalizeException("El número de documento del candidato es obligatorio");
        }
    }

    private void validateNameComplete(CandidateDto candidateDto) {
        if (candidateDto.getName() == null || candidateDto.getName().isEmpty()) {
            throw new PersonalizeException("El nombre del candidato es obligatorio");
        }

        if (candidateDto.getLastName() == null || candidateDto.getLastName().isEmpty()) {
            throw new PersonalizeException("El apellido del candidato es obligatorio");
        }
    }

    private void validateBirthDate(CandidateDto candidateDto) {
        if (candidateDto.getBirthDate() == null) {
            throw new PersonalizeException("La fecha de nacimiento del candidato es obligatoria");
        } else {
            LocalDate currentDate = LocalDate.now();
            LocalDate birthDate = candidateDto.getBirthDate();
            Period age = Period.between(birthDate, currentDate);
            if (age.getYears() < 16 || age.getYears() > 100) {
                throw new PersonalizeException("La fecha de nacimiento ingresada no es válida. Debe tener entre 16 y 100 años.");
            }
        }
    }




    private Candidate validateDataUpdate(CandidateDto candidateDto, Candidate candidate) {
        if (candidate == null) {
            throw new EntityNotFoundException(CANDIDATE_NOT_FOUND_MESSAGE);
        }

        if (candidateDto.getName() != null && !candidateDto.getName().isEmpty()) {
            candidate.setName(candidateDto.getName());
        }
        if (candidateDto.getLastName() != null && !candidateDto.getLastName().isEmpty()) {
            candidate.setLastName(candidateDto.getLastName());
        }
        if (candidateDto.getDocumentType() != null && !candidateDto.getDocumentType().isEmpty()) {
            validateDocumentType(candidateDto.getDocumentType().toUpperCase());
            candidate.setDocumentType(candidateDto.getDocumentType());
        }
        if (candidateDto.getDocumentNumber() != null && !candidateDto.getDocumentNumber().isEmpty()) {
            candidate.setDocumentNumber(candidateDto.getDocumentNumber());
        }
        if (candidateDto.getDocumentNumber() != null || candidateDto.getDocumentType() != null) {
            validateDuplicateDocument(candidate.getDocumentNumber(), candidate.getDocumentType());
        }
        if (candidateDto.getBirthDate() != null) {
            validateBirthDate(candidateDto.getBirthDate());
            candidate.setBirthDate(candidateDto.getBirthDate());
        }
        return candidate;
    }

    private void validateDocumentType(String documentType) {
        if (!(documentType.equals("DNI") || documentType.equals("LE") || documentType.equals("LC"))) {
            throw new PersonalizeException("El tipo de documento ingresado no es válido");
        }
    }

    private void validateBirthDate(LocalDate birthDate) {
        LocalDate currentDate = LocalDate.now();
        Period age = Period.between(birthDate, currentDate);
        if (age.getYears() < 16 || age.getYears() > 100) {
            throw new PersonalizeException("La fecha de nacimiento ingresada no es válida. Debe tener entre 16 y 100 años.");
        }
    }

    private void validateDuplicateDocument(String documentNumber, String documentType) {
        if (Boolean.TRUE.equals(candidateRepository.existsByDocumentNumberAndDocumentType(documentNumber, documentType))) {
            throw new PersonalizeException("Ya existe un candidato con ese tipo y número de documento");
        }
    }


    @Override
    public void addTechnology(Long candidateId, Long technologyId, CandidateTechnology candidateTechnologyYears) {
        log.info("Agregando tecnología a candidato. Tecnología ID: {}, Candidato ID: {}", technologyId, candidateId);
        Candidate candidate = candidateRepository.findById(candidateId).orElse(null);
        Technology technology = technologyRepository.findById(technologyId).orElse(null);

        if(candidate==null){
            throw new EntityNotFoundException("No se encontró el candidato");
        }

        if(technology==null){
            throw new EntityNotFoundException("No se encontró la tecnología");
        }

        if (Boolean.TRUE.equals(candidateTechnologyRepository.existsByCandidateAndTechnology(candidate, technology))) {
            throw new PersonalizeException("La combinación ya existe");
        }

        if (candidateTechnologyYears.getYearsOfExperience()==null){
            throw new PersonalizeException("Los años de experiencia no pueden ser nulos");
        }

        if (candidateTechnologyYears.getYearsOfExperience()<0) {
            throw new PersonalizeException("Los años de experiencia no pueden ser negativos");
        }

        CandidateTechnology candidateTechnology = new CandidateTechnology();
        candidateTechnology.setCandidate(candidate);
        candidateTechnology.setTechnology(technology);
        candidateTechnology.setYearsOfExperience(candidateTechnologyYears.getYearsOfExperience());
        candidateTechnologyRepository.save(candidateTechnology);

        candidate.getTechnologies().add(candidateTechnology);
        candidateRepository.save(candidate);
        log.info("Tecnología agregada con éxito al candidato");

    }


    @Override
    public List<TechnologiesAndYearsExperienceDto> getTechnologiesAndExperience(Long candidateId) {
        log.info("Buscando las tecnologías que se asocien con el id de candidato {}", candidateId);
        List<TechnologyExperienceProjection> projections = candidateTechnologyRepository.findTechnologiesAndExperience(candidateId);

        if (projections.isEmpty()) {
            throw new EntityNotFoundException("No se encontraron combinaciones con ese ID de Candidato");
        }
        log.info("Combinaciones encontradas con éxito");

        return projections.stream()
                .map(projection -> new TechnologiesAndYearsExperienceDto(technologyMapper.technologyToTechnologyDto(projection.getTechnology()), projection.getYearsOfExperience()))
                .toList();
    }
}
