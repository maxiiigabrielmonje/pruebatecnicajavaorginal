package com.example.pruebatecnica.service.service_imp;

import com.example.pruebatecnica.dto.CandidateDto;
import com.example.pruebatecnica.dto.CandidateExperienceProjection;
import com.example.pruebatecnica.dto.CandidatesAndYearsExperienceDto;
import com.example.pruebatecnica.dto.TechnologyDto;
import com.example.pruebatecnica.exception.PersonalizeException;
import com.example.pruebatecnica.mapper.CandidateMapper;
import com.example.pruebatecnica.mapper.TechnologyMapper;
import com.example.pruebatecnica.model.Candidate;
import com.example.pruebatecnica.model.CandidateTechnology;
import com.example.pruebatecnica.model.Technology;
import com.example.pruebatecnica.repository.CandidateRepository;
import com.example.pruebatecnica.repository.CandidateTechnologyRepository;
import com.example.pruebatecnica.repository.TechnologyRepository;
import com.example.pruebatecnica.service.TechnologyService;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
@Service
public class TechnologyServiceImp implements TechnologyService {

    private final TechnologyRepository technologyRepository;

    private final CandidateRepository candidateRepository;

    private final CandidateTechnologyRepository candidateTechnologyRepository;

    private final CandidateMapper candidateMapper;

    private final TechnologyMapper technologyMapper;

    private static final String TECHNOLOGY_NOT_FOUND_MESSAGE = "No se encontró una tecnología con ese ID";

    @Override
    public void saveTechnology(TechnologyDto technologyDto) {

        log.info("Validando datos...");
        validateDataCreated(technologyDto);
        Technology technology = technologyMapper.technologyDtoToTechnology(null, technologyDto);

        technology.setDateCreated(LocalDateTime.now());
        technology.setDateModified(LocalDateTime.now());
        Technology savedTechnology = technologyRepository.save(technology);

        log.info("Guardando tecnología: {}", savedTechnology.getName());
    }

    @Override
    public TechnologyDto getTechnology(Long id) {
        log.info("Obteniendo tecnología con ID: {}", id);
        Technology technology = technologyRepository.findById(id).orElse(null);

        if(technology == null){
            throw new EntityNotFoundException(TECHNOLOGY_NOT_FOUND_MESSAGE);
        }
        log.info("Tecnología obtenida con éxito");
        return technologyMapper.technologyToTechnologyDto(technology);
    }

    @Override
    public List<TechnologyDto> getAllTechnologies() {
        log.info("Obteniendo todas las tecnologías");
        List<Technology> technologies = technologyRepository.findAll();

        if(technologies.isEmpty()){
            throw new EntityNotFoundException("No se encontraron tecnologías");
        }

        List<TechnologyDto> technologyDtos = new ArrayList<>();

        for (Technology technology : technologies) {
            TechnologyDto technologyDto = technologyMapper.technologyToTechnologyDto(technology);
            technologyDtos.add(technologyDto);
        }

        log.info("Tecnologías obtenidas con éxito");
        return technologyDtos;

    }

    @Override
    public void deleteTechnology(Long id) {
        log.info("Borrando tecnología con ID: {}", id);

        if (!technologyRepository.findById(id).isPresent()) {
            throw new EntityNotFoundException(TECHNOLOGY_NOT_FOUND_MESSAGE);
        }
        technologyRepository.deleteById(id);
        log.info("Tecnología borrada con éxito");
    }

    @Override
    public void updateTechnology(Long id, TechnologyDto technologyDto) {
        log.info("Actualizando tecnología con ID: {}", id);

        Technology technologyNoValid = technologyRepository.findById(id).orElse(null);
        Technology technology = validateDataUpdate(technologyNoValid, technologyDto);

        technology.setDateModified(LocalDateTime.now());
        technologyRepository.save(technology);

        log.info("Tecnología actualizada con éxito");
    }

    private TechnologyDto validateDataCreated(TechnologyDto technologyDto){

        if(technologyDto.getName() == null || technologyDto.getName().isEmpty()){
            throw new PersonalizeException("El nombre de la tecnología es obligatorio");
        }

        if(technologyDto.getVersion() == null || technologyDto.getVersion().isEmpty()){
            throw new PersonalizeException("La versión de la tecnología es obligatoria");
        }

        if (Boolean.TRUE.equals(technologyRepository.existsByNameAndVersion(technologyDto.getName(), technologyDto.getVersion()))) {
            throw new PersonalizeException("Ya existe una tecnología con el mismo nombre y versión.");
        }

        return technologyDto;
    }

    private Technology validateDataUpdate(Technology technology, TechnologyDto technologyDto){

        if(technology==null){
            throw new EntityNotFoundException(TECHNOLOGY_NOT_FOUND_MESSAGE);
        }

        if(technologyDto.getName() != null && !technologyDto.getName().isEmpty()){
            technology.setName(technologyDto.getName());
        }

        if(technologyDto.getVersion() != null && !technologyDto.getVersion().isEmpty()){
            technology.setVersion(technologyDto.getVersion());
        }

        if (technologyDto.getVersion() != null && technologyDto.getName() != null && Boolean.TRUE.equals(technologyRepository.existsByNameAndVersion(technology.getName(), technology.getVersion()))) {
            throw new PersonalizeException("Ya existe una tecnología con el mismo nombre y versión.");
        }


        return technology;
    }

    public void addCandidate(Long technologyId, Long candidateId, CandidateTechnology candidateTechnologyYears){
        log.info("Agregando candidato a tecnología. Tecnología ID: {}, Candidato ID: {}", technologyId, candidateId);

        Technology technology = technologyRepository.findById(technologyId).orElse(null);
        Candidate candidate =  candidateRepository.findById(candidateId).orElse(null);

        if(technology == null){
            throw new EntityNotFoundException("No se encontró el candidato con ese ID");
        }

        if(candidate==null){
            throw new EntityNotFoundException("No se encontró la tecnología con ese ID");
        }

        if (Boolean.TRUE.equals(candidateTechnologyRepository.existsByCandidateAndTechnology(candidate, technology))) {
            throw new PersonalizeException("La combinación ya existe");
        }

        if(candidateTechnologyYears.getYearsOfExperience()==null){
            throw new PersonalizeException("Los años de experiencia no pueden ser nulos");
        }

        if(candidateTechnologyYears.getYearsOfExperience()<0){
            throw new PersonalizeException("Los años de experiencia no pueden ser negativos");
        }

        CandidateTechnology candidateTechnology = new CandidateTechnology();
        candidateTechnology.setCandidate(candidate);
        candidateTechnology.setTechnology(technology);
        candidateTechnology.setYearsOfExperience(candidateTechnologyYears.getYearsOfExperience());
        candidateTechnologyRepository.save(candidateTechnology);

        technology.getCandidates().add(candidateTechnology);
        technologyRepository.save(technology);
        log.info("Candidato agregado con éxito a la tecnología");
    }

    @Override
    public List<CandidatesAndYearsExperienceDto> getCandidatesAndExperience(Long technologyId) {
        log.info("Buscando los candidatos que se asocien con el id de tecnología {}", technologyId);
        List<CandidateExperienceProjection> projections = candidateTechnologyRepository.findCandidatesAndExperience(technologyId);

        if (projections.isEmpty()) {
            throw new EntityNotFoundException("No se encontraron combinaciones con ese ID de tecnología");
        }
        log.info("Combinaciones encontradas con éxito");

        return projections.stream()
                .map(projection -> new CandidatesAndYearsExperienceDto(candidateMapper.candidateToCandidateDto(projection.getCandidate()), projection.getYearsOfExperience()))
                .toList();
    }

    @Override
    public TechnologyDto getTechnologyNameAndVersion(String name, String version){

        log.info("Validando nombre y versión de la tecnología");

        if(name == null || name.isEmpty()){
            throw new PersonalizeException("El nombre no puede ser nulo");
        }

        if(version == null || version.isEmpty()){
            throw new PersonalizeException("La version no puede ser nula");
        }

        return  technologyRepository.findByNameAndVersion(name, version)
                .map(technologyMapper::technologyToTechnologyDto)
                .orElseThrow(() -> new EntityNotFoundException("No se encontró una tecnología con esa combinación de nombre y versión"));
    }

    @Override
    public List<CandidatesAndYearsExperienceDto> getCandidatesAndExperienceNV(TechnologyDto technologyDto) {
        log.info("Buscando los candidatos que se asocien con el nombre y versión de tecnología {} {}", technologyDto.getName(), technologyDto.getVersion());
        List<CandidateExperienceProjection> projections = candidateTechnologyRepository.findCandidatesAndExperienceNV(technologyDto.getName(), technologyDto.getVersion());

        if (projections.isEmpty()) {
            throw new EntityNotFoundException("No se encontraron combinaciones con ese nombre y versión de tecnología");
        }
        log.info("Combinaciones encontradas con éxito");

        return projections.stream()
                .map(projection -> new CandidatesAndYearsExperienceDto(candidateMapper.candidateToCandidateDto(projection.getCandidate()), projection.getYearsOfExperience()))
                .toList();
    }


}
