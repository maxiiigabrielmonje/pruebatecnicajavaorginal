package com.example.pruebatecnica.service;

import com.example.pruebatecnica.dto.CandidatesAndYearsExperienceDto;
import com.example.pruebatecnica.dto.TechnologyDto;
import com.example.pruebatecnica.model.CandidateTechnology;

import java.util.List;

public interface TechnologyService {

    void saveTechnology(TechnologyDto technologyDto);

    TechnologyDto getTechnology(Long id);

    List<TechnologyDto> getAllTechnologies();

    void deleteTechnology(Long id);

    void updateTechnology(Long id, TechnologyDto technologyDto);

   void addCandidate(Long technologyId, Long candidateId, CandidateTechnology candidateTechnologyYears);

    List<CandidatesAndYearsExperienceDto> getCandidatesAndExperience(Long technologyId);

    TechnologyDto getTechnologyNameAndVersion(String name, String version);

    List<CandidatesAndYearsExperienceDto> getCandidatesAndExperienceNV(TechnologyDto technologyDto);


}
