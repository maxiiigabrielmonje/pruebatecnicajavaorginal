package com.example.pruebatecnica.service;

import com.example.pruebatecnica.dto.CandidateDto;
import com.example.pruebatecnica.dto.TechnologiesAndYearsExperienceDto;
import com.example.pruebatecnica.model.CandidateTechnology;

import java.util.List;

public interface CandidateService {

    void saveCandidate(CandidateDto candidateDto);

    CandidateDto getCandidate(Long id);

    List<CandidateDto> getAllCandidates();

    void deleteCandidate(Long id);

    void updateCandidate(Long id, CandidateDto candidateDto);

    void addTechnology(Long candidateId, Long technologyId, CandidateTechnology candidateTechnology);

    List<TechnologiesAndYearsExperienceDto> getTechnologiesAndExperience(Long candidateId);
}
